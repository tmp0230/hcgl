#!python
# -*- coding: utf-8 -*-

import hashlib

import web
from sqlalchemy.orm import scoped_session, sessionmaker

from models import engine, User
from renders import render
from utils.wrapper import user_wrapper

	
def my_handler(handler):
    ''' 将session对象存储在web.ctx.session中，方便其它app使用
        将每次访问的数据库操作看做一个事务来处理
    '''
    web.ctx.session = web.config._session
    web.ctx.orm = scoped_session(sessionmaker(bind=engine))
    try:
        return handler()
    except web.HTTPError:
        web.ctx.orm.commit()
        raise
    except:
        web.ctx.orm.rollback()
        raise
    finally:
        web.ctx.orm.commit()
		
def logged():
    ''' 检查用户是否登录'''
    return True if web.ctx.session.get('user') else False
		
def login_required(func, limits=['user', 'admin', 'sysadmin']):
    ''' 装饰器，限定可操作的用户种类，默认登录即可'''
    def Function(*a, **k):
        if not logged():
            return web.seeother('/login')
        if web.ctx.session.user.category not in limits:
            return render.forbidden()
        return func(*a, **k)
    return Function
		
class redirect(object):
    ''' 对于末尾的/重定向处理'''
    def GET(self, path):
        raise web.seeother('/' + path)
		
class login(object):
    ''' 处理登录，流程见用户认证流程图'''
    def GET(self):
        if logged():
            raise web.seeother(web.ctx.session.user.mainpage)
        return render.login()
        
    def POST(self):
        i = web.input(username=None, password=None)
        if i.username and i.password:
            user = web.ctx.orm.query(User).filter_by(name=i.username).first()
            if user and hashlib.md5(i.password).hexdigest() == user.password:
                web.ctx.session.user = user_wrapper(user)
                raise web.seeother(web.ctx.session.user.mainpage)
        return render.login(error=u'用户名或密码错误！')
		
class logout(object):
    ''' 登出'''
    def GET(self):
        web.ctx.session.kill()
        raise web.seeother('/login')

class registe(object):
    '''注册'''
    def GET(self):
        if logged():
            raise web.seeother(web.ctx.session.user.mainpage)
        return render.registe()
    
    def POST(self):
        i = web.input(username=None, password=None, password2=None)
        if i.username and i.password and i.password2 and i.password==i.password2 \
                and not web.ctx.orm.query(User).filter_by(name=i.username).first():
            user = User(i.username, i.password)
            web.ctx.orm.add(user)
            try:
                web.ctx.orm.commit()
                web.ctx.session.user = user_wrapper(user)
                return render.registe_success()
            except:
                web.ctx.orm.rollback()
        return render.registe(error=u'请重新检查！')
		
class notfd(object):
    ''' 404'''
    def GET(self, path):
        return render.notfound(content=path)
    def POST(self, path):
        return render.notfound(path)
	
class user(object):
    @login_required
    def GET(self):
        return render.user(is_logged=True)
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
