#!python
# -*- coding: utf-8 -*-

import web
from web.contrib.template import render_jinja

import os

__all__ = ['render', 'render_admin']

def get_render(path):
    render = render_jinja(
        os.getcwd() + '/' + path,
        encoding = 'utf-8',
    )
    return render

render = get_render('templates')
render_admin = get_render('templates/admin')
